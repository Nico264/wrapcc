// SPDX-License-Identifier: BSD-3-Clause

/*

Copyright (C) 2002 Oswald Buddenhagen <oswald.buddenhagen@gmx.de>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the University nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

*/

/*
 * Usage:
 * - compile with gcc -O2 -o wrapcc wrapcc3.c
 * - symlink gcc/g++/cc/c++/make from /usr/local/bin (or anything else that
 *   is in PATH before the actual compiler toolchain) to this executable
 * - set up WRAPCCFLAGS:
 *   - list of comma-separated flags
 *   - "jobs=n" - pass -jn to make
 *   - "ice=n" - use icecc, and pass -jn to make
 *   - "dist" - use distcc, and pass -jn to make (n determined from DISTCC_HOSTS)
 *   - "cache" - use ccache. It is possible to specify a list of roots with local
 *     caches: cache=/root1=/cachedir1:/root2=/cachedir2. If no roots are given,
 *     the wrapper will look for .ccache/ up the directory hierarchy. If none is
 *     found, ccache's default (~/.ccache/) will be used.
 *   - "append=-suffix" - suffix to append to compiler binary. This makes it
 *     possible to invoke x-compilers without wrapping them explicitly.
 *
 *   Example: export WRAPCCFLAGS=cache,ice=40  # caching and a big icecream farm
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <alloca.h>
#include <stdarg.h>
#include <limits.h>

static void __attribute__((noreturn))
Panic( const char *msg, ... )
{
	va_list va;

	va_start( va, msg );
	vfprintf( stderr, msg, va );
	va_end( va );
	exit( 1 );
}

static int debug;

static void
Debug( const char *msg, ... )
{
	va_list va;

	if (debug) {
		va_start( va, msg );
		vfprintf( stderr, msg, va );
		va_end( va );
	}
}

char *
findpath( const char *path, const char *name0 )
{
	const char *pathe;
	char *name, *thenam;
	int len;
	char nambuf[PATH_MAX+1], resolved[PATH_MAX+1];
	static char myself[PATH_MAX+1], directory[PATH_MAX+1];

	if (!directory[0] && !getcwd( directory, sizeof(directory) ))
		Panic( "getcwd failed.\n" );
	if (!myself[0]) {
		if ((unsigned)(len = readlink( "/proc/self/exe", myself, sizeof(myself) )) > PATH_MAX)
			Panic( "readlen /proc/self/exe failed.\n" );
		myself[len] = 0;
		Debug( "i am %s\n", myself );
	}
	len = strlen( name0 );
	name = nambuf + PATH_MAX - len;
	memcpy( name, name0, len + 1 );
	*--name = '/';
	do {
		if (!(pathe = strchr( path, ':' )))
			pathe = path + strlen( path );
		len = pathe - path;
		if (!len || (len == 1 && *path == '.')) {
			len = strlen( directory );
			path = directory;
		}
		thenam = name - len;
		if (thenam >= nambuf) {
			memcpy( thenam, path, len );
			if (!realpath( thenam, resolved ))
				Debug( "%s cannot be resolved\n", thenam );
			else if (access( thenam, X_OK ))
				Debug( "%s is not executable\n", thenam );
			else if (!strcmp( resolved, myself ))
				Debug( "%s is myself\n", thenam );
			else {
				Debug( "%s is %s\n", name0, thenam );
				return strdup( thenam );
			}
		}
		path = pathe;
	} while (*path++);
	Panic( "Can't find %s in $PATH.\n", name0 );
}

#define NPARTS 2

int main( int argc, char **argv )
{
	char **nargv = alloca( sizeof(*nargv) * (NPARTS + argc + 1) );
	char *p, *p2, *path, *name0, *name1, *env;
	int nargc = 0, i, cnt;
	char cc, fc;

	if (!(env = getenv( "WRAPCCFLAGS" )))
		Debug( "%s: WRAPCCFLAGS not set!\n", *argv );
	if (env && strstr( env, "debug" ))
		debug++;
	if (!(path = getenv( "PATH" )))
		Panic( "$PATH not set.\n" );
	name0 = strrchr( *argv, '/' );
	if (name0)
		name0++;
	else
		name0 = *argv;
	Debug( "my name is %s (full %s)\n", name0, *argv );
	if (!strcmp( name0, "make" ) || !strcmp( name0, "gmake" )) {
		static char buf[8];
		nargv[nargc++] = findpath( path, name0 );
		if (!env)
			goto no_env;
		if (strstr( env, "dist" )) {
			if ((p = getenv( "DISTCC_HOSTS" ))) {
				for (cnt = 1; (p2 = strchr( p, ' ' )); cnt++, p = p2 + 1);
				Debug( "%d compile jobs requested\n", cnt );
				if (cnt > 1) {
					sprintf( buf, "-j%d", cnt );
					nargv[nargc++] = buf;
				}
			}
		} else if ((p = strstr( env, "ice=" ))) {
			cnt = atoi( p + 4 );
			goto njobs;
		} else if ((p = strstr( env, "jobs=" ))) {
			cnt = atoi( p + 5 );
		  njobs:
			Debug( "%d compile jobs requested\n", cnt );
			if (cnt > 1) {
				sprintf( buf, "-j%d", cnt );
				nargv[nargc++] = buf;
			}
		}
		if ((p = strstr( env, "cache" ))) {
			char cwd[PATH_MAX], rcwd[PATH_MAX];
			if (getcwd( cwd, sizeof(cwd) ) && realpath( cwd, rcwd )) {
				Debug("real dir is %s\n", rcwd);
				if (p[5] == ':') {
					p += 6;
					for (;;) {
						p2 = rcwd;
						Debug("trying %s\n", p);
						for (;;) {
							cc = *p++;
							fc = *p2++;
							if (!cc || cc == ',')
								goto dirdone;
							if (cc == '=')
								break;
							if (cc != fc)
								goto notdir;
						}
						if (!fc || fc == '/') {
							for (p2 = cwd; (cc = *p++) && cc != ':' && cc != ',';)
								*p2++ = cc;
							strcpy( p2, "/.ccache" );
							setenv( "CCACHE_DIR", cwd, 1 );
							Debug( "setting cache dir to %s\n", cwd );
							goto dirdone;
						}
					    notdir:
						while ((cc = *p++) && cc != ':')
							if (!cc || cc == ',')
								goto dirdone;
					}
				   dirdone: ;
				} else {
					p = rcwd + strlen( rcwd );
					do {
						strcpy( p, "/.ccache" );
						if (!access( rcwd, X_OK )) {
							setenv( "CCACHE_DIR", rcwd, 1 );
							Debug( "setting cache dir to %s\n", rcwd );
							break;
						}
						p = memrchr( rcwd, '/', p - rcwd - 1 );
					} while (p && p != rcwd);
				}
			}
		}
	} else {
		if (!strchr( name0, '-' ) && env && (p = strstr( env, "append=" ))) {
			static char buf[16];
			for (p2 = p + 7; *p2 && *p2 != ','; p2++);
			snprintf( buf, sizeof(buf), "%s-%.*s", name0, (int)(p2 - (p + 7)), p + 7 );
			name1 = buf;
		} else
			name1 = name0;
		Debug( "target name is %s\n", name1 );
		if (env && strstr( env, "cache" )) {
			nargv[nargc++] = findpath( path, "ccache" );
			if (strstr( env, "dist" )) {
				char buf[8];
				sprintf( buf, "%d", dup( 2 ) );
				setenv( "UNCACHED_ERR_FD", buf, 1 );
				setenv( "CCACHE_PREFIX", findpath( path, "distcc" ), 1 );
			} else if (strstr( env, "ice" )) {
				char buf[8];
				sprintf( buf, "%d", dup( 2 ) );
				setenv( "UNCACHED_ERR_FD", buf, 1 );
				setenv( "CCACHE_PREFIX", findpath( path, "icecc" ), 1 );
			}
		} else if (env) {
			if (strstr( env, "dist" ))
				nargv[nargc++] = findpath( path, "distcc" );
			else if (strstr( env, "ice" ))
				nargv[nargc++] = findpath( path, "icecc" );
		}
		nargv[nargc++] = findpath( path, name1 );
	}
no_env:
	do {
		nargv[nargc++] = *++argv;
	} while (*argv);
	for (i = 0; nargv[i]; i++)
		Debug( "%s ", nargv[i] );
	Debug( "\n" );
	execvp( *nargv, nargv );
	Panic( "Cannot exec %s!\n", *nargv );
}
